import javax.print.attribute.standard.ReferenceUriSchemesSupported;
import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   private long numerator;
   private long denominator;

   /** Main method. Different tests. */
   public static void main (String[] param) {
      //Lfraction lfraction = new Lfraction(2, 4);
      System.out.println(Lfraction.valueOf("2/x"));
   }

   // TODO!!! instance variables here

   public Lfraction (long numerator, long denominator) {


      if(denominator < 0 && numerator < 0){
         denominator = -denominator;
         numerator = -numerator;
      }
      else if(denominator < 0 && numerator > 0)
      {
         numerator = -numerator;
         denominator = -denominator;
      }
      if(denominator <= 0 )
         throw new IllegalArgumentException(String.format("%d/%d is not a valid fraction. Denominator must be > 0", numerator, denominator));

      long greatestCommonDivisor = greatestCommonDivisor(numerator, denominator);
      this.numerator = numerator / greatestCommonDivisor;
      this.denominator = denominator / greatestCommonDivisor;


   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return String.format("%d/%d", getNumerator(), getDenominator());
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {

      return compareTo((Lfraction)m) == 0;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return this.toString().hashCode();
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {

      long leastCommonMultiple = leastCommonMultiple(getDenominator(), m.getDenominator());
      return new Lfraction((getNumerator() * leastCommonMultiple / getDenominator()) +
              (m.getNumerator() * leastCommonMultiple / m.getDenominator()), leastCommonMultiple);

   }


   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      return new Lfraction(getNumerator() * m.getNumerator(), getDenominator() * m.getDenominator());
      //return fraction.simplifyFraction();
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      long newNumerator, newDenominator;

      newDenominator = Math.abs(getNumerator());
      newNumerator = (getNumerator() < 0) ? -getDenominator() : getDenominator();
      if(newDenominator == 0)
         throw new RuntimeException(String.format("Can't inverse fraction %s because new denominator must not be 0."));
      return new Lfraction(newNumerator, newDenominator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-getNumerator(), getDenominator());
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {

      m = m.opposite();
      long leastCommonMultiple = leastCommonMultiple(getDenominator(), m.getDenominator());
      return new Lfraction((getNumerator() * (leastCommonMultiple / getDenominator())) +
              (m.getNumerator() * (leastCommonMultiple / m.getDenominator())), leastCommonMultiple);
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if(m.getNumerator() == 0)
         throw new IllegalArgumentException("Your argument fraction value is 0. Can't divide by 0.");

      m = m.inverse();

      return new Lfraction(getNumerator() * m.getNumerator(), getDenominator() * m.getDenominator());
      //return fraction.simplifyFraction();
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      //if(equals(m)) return 0;
      //(getNumerator() * ((Lfraction)m).getDenominator()) == (getDenominator() * ((Lfraction)m).getNumerator();
      long a = getNumerator() * ((Lfraction)m).getDenominator();
      long b = getDenominator() * ((Lfraction)m).getNumerator();
      //return (getNumerator() * m.getDenominator() > m.getNumerator() * getDenominator()) ? 1 : -1;

      if(a == b)
         return 0;
      if(a > b)
         return 1;
      else
         return -1;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(numerator, denominator);// TODO!!!
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return getNumerator() / getDenominator();
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return new Lfraction(getNumerator() % getDenominator(), getDenominator());
      //return lfraction.simplifyFraction();
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double)getNumerator() / getDenominator();
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction((long)Math.ceil(f*d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      if(!s.matches("-?(\\d)+\\/-?(\\d)+"))
         throw new IllegalArgumentException(String.format("%s is not a valid fraction string", s));
      String [] numbers = s.split("/");

      long numerator = Integer.parseInt(numbers[0]);
      long denominator = Integer.parseInt(numbers[1]);

      return new Lfraction(numerator, denominator);

   }

   public Lfraction simplifyFraction(){
      long greatestCommonDivisor = greatestCommonDivisor(getNumerator(), getDenominator());
      return new Lfraction(getNumerator() / greatestCommonDivisor, getDenominator() / greatestCommonDivisor);
   }

   //suurim ühistegur
   //https://stackoverflow.com/questions/6618994/simplifying-fractions-in-java
   public static long greatestCommonDivisor(long a, long b) {
      return b == 0 ? Math.abs(a) : greatestCommonDivisor(b, a % b);
   }

   //vähim ühiskordne
   public static long leastCommonMultiple(long a, long b){
      return (a*b) / greatestCommonDivisor(a, b);
   }

}

